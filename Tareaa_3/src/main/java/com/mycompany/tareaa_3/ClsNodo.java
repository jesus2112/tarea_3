/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tareaa_3;

/**
 *
 * @author Jesús21
 */
public class ClsNodo {
    protected int   valor;
    protected ClsNodo  nodoIzquierdo;
    protected ClsNodo  nodoDerecho;

    public ClsNodo(int data,ClsNodo izq,ClsNodo der){
        this.valor=data;
        this.nodoIzquierdo=izq;
        this.nodoDerecho=der;
    }
}
