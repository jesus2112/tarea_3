/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tareaa_3;

/**
 *
 * @author Jesús21
 */

public class ArbolBinarioPrincipal {
    ClsNodo root;

    public class ClsNodo {

        protected int valor;
        protected ClsNodo nodoIzquierdo, nodoDerecho;
    }

    public void insertar(int data) {
        ClsNodo newNode = new ClsNodo();
        if (this.root == null) { 
            this.root = newNode;
        } else {
            this.insertarNodo(this.root, newNode);
        }
  
    }
    
    public void insertarNodo(ClsNodo node, ClsNodo newNode) {
        if (newNode.valor < node.valor) {
            if (node.nodoIzquierdo == null) { 
                node.nodoIzquierdo = newNode;
            } else {
                this.insertarNodo(node.nodoDerecho, newNode);
            }
        } else {
            if (node.nodoDerecho == null) {
                node.nodoDerecho = newNode;
            } else {
                this.insertarNodo(node.nodoDerecho, newNode);
            }
        }
    }

   

    public void inOrder(ClsNodo localNode) {

        if (localNode != null) {

            inOrder(localNode.nodoIzquierdo);
            System.out.println(localNode.valor);
            inOrder(localNode.nodoDerecho);

        }
    }
    
    
  

    public void preOrder(ClsNodo localNode) {

        if (localNode != null) {
            System.out.println(localNode.valor);
            preOrder(localNode.nodoIzquierdo);
            preOrder(localNode.nodoDerecho);
        }
    }

    
    
    
    public void posOrder(ClsNodo localNode) {

        if (localNode != null) {
            posOrder(localNode.nodoIzquierdo);
            posOrder(localNode.nodoDerecho);
            System.out.println(localNode.valor);
        }
    }
    
    
    
    
    
    public ClsNodo buscar(int valor) {

    ClsNodo actual = root;

    while(actual.valor != valor) {

        if(valor < actual.valor) {
            actual = actual.nodoIzquierdo;
        }
        else {
            actual = actual.nodoDerecho;
        }
        if(actual == null) {
            return null;
        }
    }

    return actual;  
}

    public static void main(String[] args) {
     
        
    }
}